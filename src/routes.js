const express = require('express')
const router = express()
const cors = require('cors')
const SessionController = require('./app/controllers/SessionController')
const LoginCommand = require('./app/commands/login')
const RetrieveLogCommand = require('./app/commands/retrieveLog')
const FindLogCommand = require('./app/commands/findLog')
const auth = require('./app/middleware/auth')
const steam = require('steam-login')
const streamParser = require('./utils/streamParser')

router.use(streamParser)
router.use(cors())
router.use(require('express-session')({ resave: false, saveUninitialized: false, secret: 'a secret' }))
router.use(steam.middleware({
  realm: 'http://localhost:8081',
  verify: 'http://localhost:8081/verify',
  apiKey: process.env.STEAM_KEY
}
))

router.get('/login', LoginCommand.store)
router.post('/retrieveLog/', RetrieveLogCommand.store)
router.post('/sessions', SessionController.store)
router.post('/findLog', FindLogCommand.store)

router.get('/dashboard', auth, (req, res) => {
  return res.status(200).send()
})

router.get('/', function (req, res) {
  res.send(req.user == null ? 'not logged in' : 'hello ' + req.user.username).end()
})

router.get('/authenticate', steam.authenticate(), function (req, res) {
  res.redirect('/')
})

router.get('/verify', steam.verify(), function (req, res) {
  LoginCommand.store(req, res)
})

router.get('/logout', steam.enforceLogin('/'), function (req, res) {
  req.logout()
  res.redirect('/')
})

module.exports = router
