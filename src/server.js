const app = require('./app')
app.listen(8081)
console.log('Rodando na porta 8081')
const config = require('./config')

const mongoose = require('mongoose')
mongoose.connect(config.mongoServerUrl, { useNewUrlParser: true, useUnifiedTopology: true })
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', async () => {
  /*
    * Cria Schema usando strict: false, o que significa ser possível adicionar novos properties
    * */
  const kittySchema = new mongoose.Schema({
    name: String,
    accuracy: Number
  }, { strict: false })

  const playerCollection = mongoose.model('players', kittySchema) // Cria collection players
  /* await playerCollection.create({ name: 'firstPlayer', teste: 'aff' }) */ // Cria novo documento
  const firstPlayer = await playerCollection.find({ name: 'firstPlayer' }) // Procura por documento com nome "firstPlayer"
  if (firstPlayer.length) {
    await firstPlayer[0].updateOne({ $set: { testeDeUpdateUnico: 'tesadasz' } }) // Atualiza documento especifico
  }
  await playerCollection.updateMany({}, { $set: { ALOOOO: 'foo' } }) // Atualiza TODOS DOCUMENTOS
})
