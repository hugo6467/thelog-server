function parseStream (req, res, next) {
  if (req.body === undefined || Object.keys(req.body).length === 0) {
    const buffer = []
    req.on('data', function onRequestData (chunk) {
      buffer.push(chunk)
    })

    req.once('end', function () {
      const concated = Buffer.concat(buffer)
      req.body = concated.toString('utf8') // change it to meet your needs (gzip, json, etc)
      next()
    })
  } else {
    next()
  }
}
module.exports = parseStream
