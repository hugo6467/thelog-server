const mongoose = require('mongoose')
const config = require('../../config')

class FindLog {
  async store (req, res) {
    const params = req.body
    mongoose.connect(config.mongoServerUrl, { useNewUrlParser: true, useUnifiedTopology: true })
    const playerModel = mongoose.model('players')
    try {
      const requestParam = params.steamID ? { steamID: params.steamID } : {}
      const foundPlayer = await playerModel.find(requestParam)
      console.log(foundPlayer)
      return res.json({
        retrievedLog: foundPlayer
      })
    } catch (err) {
      console.error(err)
    }
  }
}

module.exports = new FindLog()
