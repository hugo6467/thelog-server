const axios = require('axios')
const mongoose = require('mongoose')
const config = require('../../config')
/* const { Logs } = require("../models"); */
/* new Date(1559 * 1000).toISOString().substr(11, 8) */ // Para formatar data
// eslint-disable-next-line require-jsdoc
class Login {
  // eslint-disable-next-line require-jsdoc
  async store (req, res) {
    const retrievedLog = await axios.get(`http://logs.tf/api/v1/log${req.query.logId}`)
    let players = []

    mongoose.connect(config.mongoServerUrl, { useNewUrlParser: true, useUnifiedTopology: true })
    const playerModel = mongoose.model('players')

    players = Object.entries(retrievedLog.data.players)
    for (const player of players) {
      const currentPlayerChat = []
      for (const playerChat of retrievedLog.data.chat) {
        if (player[0] === playerChat.steamid) {
          currentPlayerChat.push(playerChat.msg)
        }
      }
      const foundPlayer = await playerModel.find({ steamID: player[0] })
      if (foundPlayer.length) {
        await playerModel.updateOne({ steamID: player[0] },
          {
            $addToSet:
                       {
                         playerNames: retrievedLog.data.names[player[0]],
                         matches: {
                           [req.query.logId]:
                               {
                                 stats: player[1],
                                 matchInfo: retrievedLog.data.info,
                                 matchChat: currentPlayerChat
                               }
                         }
                       }
          })
      } else {
        await playerModel.create({
          steamID: player[0],
          playerNames: [retrievedLog.data.names[player[0]]],
          matches: [{
            [req.query.logId]: {
              stats: player[1],
              matchInfo: retrievedLog.data.info,
              matchChat: currentPlayerChat
            }
          }]
        })
      }
    }

    // players = Object.entries(retrievedLog.data.players)
    /* const user = await Logs.create({
            logid: req.query.logId,
            players
        }) */
    return res.json({
      retrievedLog: retrievedLog.data,
      status: retrievedLog.status
    })
  }
}

module.exports = new Login()
