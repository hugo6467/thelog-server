const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  const Logs = sequelize.define(
    "Logs",
    {
      logid: DataTypes.STRING,
      players: DataTypes.ARRAY(DataTypes.STRING)
    }
  );

  return Logs;
};
