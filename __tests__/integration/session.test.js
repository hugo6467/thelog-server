const request = require("supertest")
const app = require("../../src/app")
const truncate = require("../utils/truncate")
const factory = require("../factories")

describe("Login", () => {
  beforeEach(async () => {
    await truncate()
  })
  it('should login with correct password', async () => {
    const user = await factory.create('User', {
      password: '123'
    })

    const response = await request(app)
        .post('/sessions')
        .send({
          email: user.email,
          password: '123'
        })
    expect(response.status).toBe(200)
  })

  it('should be able to acess private routes', async() => {
      const user = await factory.create('User', {
          password: '123'
      })
      const response = await request(app)
          .get('/dashboard')
          .set('Authorization', `Bearer ${user.generateToken()}`)

      expect(response.status).toBe(200)
  })
})
